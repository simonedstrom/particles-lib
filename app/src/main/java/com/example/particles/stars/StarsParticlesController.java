package com.example.particles.stars;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import java.util.Iterator;
import java.util.List;

import se.snylt.particles.MathUtils;
import se.snylt.particles.Particle;
import se.snylt.particles.ParticlesWorld;
import se.snylt.particles.ParticlesWorldController;


public class StarsParticlesController implements ParticlesWorldController<StarParticle> {

    private final int maxThings = 30;

    private final long spawnFrequencyInMillis = 700;

    private final int spawnCount = 1;

    private long spawnGuiltInMillis;

    private final Drawable image;

    public StarsParticlesController(Drawable image) {
        this.image = image;
    }

    private StarParticle createParticle(ParticlesWorld particlesWorld, float x) {
        Rect bounds = particlesWorld.getBounds();
        float y = (float) (bounds.height() * Math.random());
        return new StarParticle(new PointF(x, y), new PointF( -60, (float) MathUtils.random(-2d, 2d)), image);
    }

    @Override
    public void onWorldStarted(ParticlesWorld particlesWorld) {
        List<Particle> particles = particlesWorld.getParticles(StarParticle.class);

        // Add 20 stars
        for (int i = 0; i < 20; i++) {
            particles.add(createParticle(particlesWorld, (float) (particlesWorld.getBounds().width() * Math.random())));
        }
    }

    @Override
    public void onUpdateWorld(ParticlesWorld particlesWorld, List<StarParticle> particles, long dt) {

        // PRE
        if(particles.size() < maxThings) {
            spawnGuiltInMillis += dt;
        }

        if (particles.size() <= maxThings && spawnGuiltInMillis > spawnFrequencyInMillis) {
            for(int i = 0; i < spawnCount; i++) {
                particles.add(createParticle(particlesWorld, particlesWorld.getBounds().width()));
            }
            spawnGuiltInMillis -= spawnFrequencyInMillis;
        }

        // UPDATE
        for(StarParticle particle: particles) {
            particle.position.y += (particle.force.y * ((dt* particlesWorld.getSizeUnit())/ 1000f));
            particle.position.x += (particle.force.x * ((dt* particlesWorld.getSizeUnit())/ 1000f));
        }

        // POST
        Iterator<StarParticle> iterator = particles.iterator();
        while (iterator.hasNext()) {
            StarParticle p = iterator.next();
            if (!particlesWorld.getBounds().contains((int) p.position.x, (int) p.position.y)) {
                iterator.remove();
            }
        }
    }

    @Override
    public void onWorldStopped(ParticlesWorld particlesWorld) {

    }


    @Override
    public Class<StarParticle> getParticleClass() {
        return StarParticle.class;
    }
}