package com.example.particles.stars;

import android.graphics.PointF;
import android.graphics.drawable.Drawable;

import se.snylt.particles.Particle;

public class StarParticle extends Particle {

    public PointF force;

    public StarParticle(PointF position, PointF force, Drawable image) {
        super(position, image);
        this.force = force;
    }

}
