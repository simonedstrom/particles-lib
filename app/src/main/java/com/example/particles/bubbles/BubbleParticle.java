package com.example.particles.bubbles;

import android.graphics.PointF;
import android.graphics.drawable.Drawable;

import se.snylt.particles.Particle;

public class BubbleParticle extends Particle {

    public PointF force;

    public BubbleParticle(PointF position, PointF force, Drawable image) {
        super(position, image);
        this.force = force;
    }

}
