package com.example.particles.bubbles;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import java.util.Iterator;
import java.util.List;

import se.snylt.particles.MathUtils;
import se.snylt.particles.ParticlesWorld;
import se.snylt.particles.ParticlesWorldController;

public class BubbleParticlesController implements ParticlesWorldController<BubbleParticle> {

    private final int maxThings = 30;

    private final long spawnFrequencyInMillis = 700;

    private final int spawnCount = 1;

    private long spawnGuiltInMillis;

    private final Drawable image;

    BubbleParticlesController(Drawable image) {
        this.image = image;
    }

    private BubbleParticle createParticle(ParticlesWorld particlesWorld) {
        Rect bounds = particlesWorld.getBounds();
        float x = (float) (bounds.width() * Math.random());
        float y = bounds.height();
        return new BubbleParticle(new PointF(x, y), new PointF((float) MathUtils.random(-4d, 4d), -60), image);
    }


    @Override
    public void onWorldStarted(ParticlesWorld particlesWorld) {

    }

    @Override
    public void onUpdateWorld(ParticlesWorld particlesWorld, List<BubbleParticle> particles, long dt) {
        // PRE
        if(particles.size() < maxThings) {
            spawnGuiltInMillis += dt;
        }

        if (particles.size() <= maxThings && spawnGuiltInMillis > spawnFrequencyInMillis) {
            for(int i = 0; i < spawnCount; i++) {
                particles.add(createParticle(particlesWorld));
            }
            spawnGuiltInMillis -= spawnFrequencyInMillis;
        }

        // UPDATE
        for(BubbleParticle particle: particles) {
            particle.position.y += (particle.force.y * ((dt* particlesWorld.getSizeUnit())/ 1000f));
            particle.position.x += (particle.force.x * ((dt* particlesWorld.getSizeUnit())/ 1000f));
        }

        // POST
        Iterator<BubbleParticle> iterator = particles.iterator();
        while (iterator.hasNext()) {
            BubbleParticle p = iterator.next();
            if (!particlesWorld.getBounds().contains((int) p.position.x, (int) p.position.y)) {
                iterator.remove();
            }
        }
    }

    @Override
    public void onWorldStopped(ParticlesWorld particlesWorld) {

    }

    @Override
    public Class<BubbleParticle> getParticleClass() {
        return null;
    }
}