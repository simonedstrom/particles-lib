package com.example.particles.rain;

import android.graphics.PointF;
import android.graphics.drawable.Drawable;

import se.snylt.particles.Particle;

public class RainParticle extends Particle {

    public PointF force;

    public RainParticle(PointF position, PointF force, Drawable image) {
        super(position, image);
        this.force = force;
    }

}
