package com.example.particles.rain;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import java.util.Iterator;
import java.util.List;

import se.snylt.particles.ParticlesWorld;
import se.snylt.particles.ParticlesWorldController;

public class RainParticlesController implements ParticlesWorldController<RainParticle> {

    private final int maxThings = 100;

    private final long spawnFrequencyInMillis = 120;

    private final int spawnCount = 2;

    private long spawnGuiltInMillis;

    private final Drawable image;

    public RainParticlesController(Drawable image) {
        this.image = image;
    }

    private RainParticle createParticle(ParticlesWorld particlesWorld) {
        Rect bounds = particlesWorld.getBounds();
        float x = (float) (((float) bounds.width()) * Math.random());
        float y = 0;
        return new RainParticle(new PointF(x, y), new PointF(0, 1000), image);
    }

    @Override
    public void onWorldStarted(ParticlesWorld particlesWorld) {

    }

    @Override
    public void onUpdateWorld(ParticlesWorld particlesWorld, List<RainParticle> particles, long dt) {
        // PRE
        if(particles.size() < maxThings) {
            spawnGuiltInMillis += dt;
        }

        if (particles.size() <= maxThings && spawnGuiltInMillis > spawnFrequencyInMillis) {
            for(int i = 0; i < spawnCount; i++) {
                particles.add(createParticle(particlesWorld));
            }
            spawnGuiltInMillis -= spawnFrequencyInMillis;
        }

        // UPDATE
        for(RainParticle particle: particles) {
            particle.position.y += (particle.force.y * ((dt* particlesWorld.getSizeUnit())/ 1000f));
            particle.position.x += (particle.force.x * ((dt* particlesWorld.getSizeUnit())/ 1000f));
        }

        // POST
        Iterator<RainParticle> iterator = particles.iterator();
        while (iterator.hasNext()) {
            RainParticle p = iterator.next();
            if (!particlesWorld.getBounds().contains((int) p.position.x, (int) p.position.y)) {
                iterator.remove();
            }
        }
    }

    @Override
    public void onWorldStopped(ParticlesWorld particlesWorld) {

    }

    @Override
    public Class<RainParticle> getParticleClass() {
        return RainParticle.class;
    }
}