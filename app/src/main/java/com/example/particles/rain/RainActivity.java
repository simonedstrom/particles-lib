package com.example.particles.rain;


import com.example.particles.R;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import se.snylt.particles.ParticlesDrawable;


public class RainActivity extends AppCompatActivity {

    ParticlesDrawable drawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);

        final Drawable drop = ContextCompat.getDrawable(RainActivity.this, R.drawable.rain_drop);
        drawable = new ParticlesDrawable(this, new RainParticlesController(drop));
        drawable.setUpdateFrequency(33);

        View view = findViewById(R.id.content);
        ViewCompat.setBackground(view, drawable);
    }

    @Override
    protected void onStart() {
        super.onStart();
        drawable.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        drawable.stop();
    }
}

