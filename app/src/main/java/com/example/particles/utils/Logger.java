package com.example.particles.utils;


import android.os.SystemClock;
import android.util.Log;

import java.util.HashMap;

public class Logger {

    private final static Logger logger = new Logger();

    private HashMap<String, Long> startTimes = new HashMap<>();

    public static void startTimer(String key) {
        logger.start(key);
    }

    private void start(String key) {
        startTimes.put(key, SystemClock.uptimeMillis());
    }

    public static void stopTimer(String key) {
        d("Timer", key + ": " + logger.stop(key) + " ms");
    }

    private long stop(String key) {
        long startTime = startTimes.get(key);
        startTimes.remove(key);
        return SystemClock.uptimeMillis() - startTime;
    }

    public static void d(String tag, String message) {
        logger.logD(tag, message);
    }

    private void logD(String tag, String message) {
        Log.d(tag, message);
    }

}
