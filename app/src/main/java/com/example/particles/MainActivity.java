package com.example.particles;

import com.example.particles.bubbles.BubblesActivity;
import com.example.particles.rain.RainActivity;
import com.example.particles.smoke.SmokeActivity;
import com.example.particles.stars.StarsActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.smoke_button).setOnClickListener(this);
        findViewById(R.id.rain_button).setOnClickListener(this);
        findViewById(R.id.bubbles_button).setOnClickListener(this);
        findViewById(R.id.starts_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.smoke_button:
                startActivity(new Intent(this, SmokeActivity.class));
                break;
            case R.id.rain_button:
                startActivity(new Intent(this, RainActivity.class));
                break;
            case R.id.bubbles_button:
                startActivity(new Intent(this, BubblesActivity.class));
                break;
            case R.id.starts_button:
                startActivity(new Intent(this, StarsActivity.class));
                break;
        }
    }
}
