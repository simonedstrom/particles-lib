package com.example.particles.smoke;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import java.util.Iterator;
import java.util.List;

import se.snylt.particles.MathUtils;
import se.snylt.particles.ParticlesWorld;
import se.snylt.particles.ParticlesWorldController;

public class SmokeParticlesController implements ParticlesWorldController<SmokeParticle> {

    private final int maxParticles;

    private final long spawnFrequencyInMillis;

    private final int spawnCount;

    private long spawnGuiltInMillis;

    private final Drawable image;

    private final float originX;

    private final float originY;

    public SmokeParticlesController(Drawable image, int maxParticles, int spawnFrequencyInMillis, int spawnCount,
        float originX, float originY) {
        this.image = image;
        this.maxParticles = maxParticles;
        this.spawnFrequencyInMillis = spawnFrequencyInMillis;
        this.spawnCount = spawnCount;
        this.originX = originX;
        this.originY = originY;
    }

    private SmokeParticle createParticle(ParticlesWorld particlesWorld) {
        long timeToLive = 4000;
        Rect bounds = particlesWorld.getBounds();
        PointF spawnPoint = new PointF(bounds.width() * originX, bounds.height() * originY);
        PointF spawnForce = new PointF(0f, -70f);
        PointF spawnForceVariance = new PointF(20f, 20f);
        PointF forceWithVariance = new PointF(spawnForce.x + (float) MathUtils
            .random(-spawnForceVariance.x, spawnForceVariance.x),
            spawnForce.y + (float) MathUtils.random(spawnForceVariance.y, spawnForceVariance.y));
        return new SmokeParticle(spawnPoint, forceWithVariance, image, timeToLive);
    }

    @Override
    public void onWorldStarted(ParticlesWorld particlesWorld) {

    }

    @Override
    public void onUpdateWorld(ParticlesWorld particlesWorld, List<SmokeParticle> particles, long dt) {

        // PRE
        if (particles.size() < maxParticles) {
            spawnGuiltInMillis += dt;
        }

        if (particles.size() <= maxParticles && spawnGuiltInMillis > spawnFrequencyInMillis) {
            for (int i = 0; i < spawnCount; i++) {
                particles.add(createParticle(particlesWorld));
            }
            spawnGuiltInMillis -= spawnFrequencyInMillis;
        }

        // UPDATE
        for(SmokeParticle particle : particles) {
            particle.position.y += (particle.force.y * ((dt* particlesWorld.getSizeUnit())/ 1000f));
            particle.position.x += (particle.force.x * ((dt* particlesWorld.getSizeUnit())/ 1000f));
            particle.timeToLive -= dt;
            particle.alpha = Math.max(0, (int) (200 * ((1f * particle.timeToLive) / particle.lifeTime * 1f)));
        }

        // POST
        Iterator<SmokeParticle> iterator = particles.iterator();
        while (iterator.hasNext()) {
            SmokeParticle p = iterator.next();
            if (p.alpha <= 0 || !particlesWorld.getBounds().contains((int) p.position.x, (int) p.position.y)) {
                iterator.remove();
            }
        }
    }

    @Override
    public void onWorldStopped(ParticlesWorld particlesWorld) {

    }

    @Override
    public Class<SmokeParticle> getParticleClass() {
        return SmokeParticle.class;
    }
}
