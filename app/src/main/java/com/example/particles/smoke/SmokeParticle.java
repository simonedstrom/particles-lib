package com.example.particles.smoke;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;

import se.snylt.particles.Particle;


public class SmokeParticle extends Particle {

    public long timeToLive;

    public final long lifeTime;

    public int alpha;

    public PointF force;

    public SmokeParticle(PointF position, PointF force, Drawable image, long timeToLive) {
        super(position, image);
        this.force = force;
        this.timeToLive = timeToLive;
        this.lifeTime = timeToLive;
    }

    @Override
    public void onDraw(Canvas canvas) {
        image.setAlpha(alpha);
        super.onDraw(canvas);
    }

}
