package com.example.particles.smoke;

import com.example.particles.R;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import se.snylt.particles.ParticlesDrawable;

public class SmokeActivity extends AppCompatActivity {

    ParticlesDrawable drawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);

        Drawable smokeParticle = ContextCompat.getDrawable(this, R.drawable.smoke);
        drawable = new ParticlesDrawable(this, new SmokeParticlesController(smokeParticle, 100, 20, 1, 0.5f, 0.8f));
        drawable.setUpdateFrequency(33);

        View view = findViewById(R.id.content);
        ViewCompat.setBackground(view, drawable);
    }

    @Override
    protected void onStart() {
        super.onStart();
        drawable.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        drawable.stop();
    }
}
