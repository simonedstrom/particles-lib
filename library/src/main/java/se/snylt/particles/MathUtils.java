package se.snylt.particles;

public class MathUtils {

    public static double random(double lowerInclusive, double upperInclusive) {
        double d = upperInclusive - lowerInclusive;
        return lowerInclusive + d*Math.random();
    }

    public static double random(float lowerInclusive, float upperInclusive) {
        return random((double) lowerInclusive, (double) upperInclusive);
    }

    public static double random(int lowerInclusive, int upperInclusive) {
        return random((double) lowerInclusive, (double) upperInclusive);
    }
}
