package se.snylt.particles.scheduler;


public interface UpdateScheduler {

    void scheduleUpdateDelayed(long delayMillis);

    void cancelScheduledUpdate();

    public interface UpdateCallback {

        void onUpdate();
    }
}
