package se.snylt.particles.scheduler;


import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.Choreographer;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class ChoreographerScheduler implements UpdateScheduler {

    private final FrameCallback callback;

    public ChoreographerScheduler(UpdateCallback updateCallback) {
        this.callback = new FrameCallback(updateCallback);
    }

    public void scheduleUpdateDelayed(long delayMillis) {
        Choreographer.getInstance().postFrameCallbackDelayed(callback, delayMillis);
    }

    @Override
    public void cancelScheduledUpdate() {
        callback.stopped = true;
        Choreographer.getInstance().removeFrameCallback(callback);
    }

    private final class FrameCallback implements Choreographer.FrameCallback {

        private final UpdateCallback updateCallback;

        boolean stopped = false;

        private FrameCallback(UpdateCallback updateCallback) {
            this.updateCallback = updateCallback;
        }

        @Override
        public void doFrame(long frameTimeNanos) {
            if(!stopped) {
                updateCallback.onUpdate();
            }
        }
    }
}
