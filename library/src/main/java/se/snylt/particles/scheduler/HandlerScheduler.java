package se.snylt.particles.scheduler;


import android.os.Handler;

public class HandlerScheduler implements UpdateScheduler {

    private final RunnableCallback callback;

    private final Handler handler;

    public HandlerScheduler(UpdateCallback updateCallback) {
        this.handler = new Handler();
        this.callback = new RunnableCallback(updateCallback);
    }

    public void scheduleUpdateDelayed(long delayMillis) {
        handler.postDelayed(callback, delayMillis);
    }

    @Override
    public void cancelScheduledUpdate() {
        handler.removeCallbacks(callback);
    }

    private final class RunnableCallback implements Runnable {

        private final UpdateCallback updateCallback;

        private RunnableCallback(UpdateCallback updateCallback) {
            this.updateCallback = updateCallback;
        }

        @Override
        public void run() {
            updateCallback.onUpdate();
        }
    }

}
