package se.snylt.particles;

import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

import se.snylt.particles.scheduler.ChoreographerScheduler;
import se.snylt.particles.scheduler.HandlerScheduler;
import se.snylt.particles.scheduler.UpdateScheduler;

public class UpdateRunner implements UpdateScheduler.UpdateCallback {

    private final ParticlesWorld particlesWorld;

    private final ParticlesDrawable drawable;

    private final UpdateScheduler scheduler;

    private final long updateFrequency;

    private long lastUpdate;

    public UpdateRunner(ParticlesWorld particlesWorld, ParticlesDrawable drawable, long updateFrequency) {
        this.particlesWorld = particlesWorld;
        this.updateFrequency = updateFrequency;
        this.drawable = drawable;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scheduler = new ChoreographerScheduler(this);
        } else {
            scheduler = new HandlerScheduler(this);
        }
    }

    public void onStart() {
        Log.d("RUNNER", "onStart()");
        lastUpdate = SystemClock.uptimeMillis();
        particlesWorld.onStart();
        this.scheduler.scheduleUpdateDelayed(0);
    }

    public void onStop() {
        Log.d("RUNNER", "onStop()");
        this.scheduler.cancelScheduledUpdate();
        particlesWorld.onStop();
    }

    @Override
    public void onUpdate() {
        long updateLatency = SystemClock.uptimeMillis();
        particlesWorld.onUpdate(SystemClock.uptimeMillis() - lastUpdate);
        drawable.invalidateSelf();
        updateLatency = SystemClock.uptimeMillis() - updateLatency;
        lastUpdate = SystemClock.uptimeMillis();
        scheduler.scheduleUpdateDelayed(Math.max(0, updateFrequency - updateLatency));

    }
}
