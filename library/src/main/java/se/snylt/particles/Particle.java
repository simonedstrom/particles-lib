package se.snylt.particles;


import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;

public abstract class Particle {

    public PointF position;

    public Drawable image;

    public Particle(PointF position, Drawable image) {
        this.position = position;
        this.image = image;
        this.image.setBounds(-image.getIntrinsicWidth()/2, -image.getIntrinsicHeight()/2, image.getIntrinsicWidth()/2, image.getIntrinsicHeight()/2);
    }

    public void onDraw(Canvas canvas) {
        canvas.translate(position.x, position.y);
        image.draw(canvas);
    }
}
