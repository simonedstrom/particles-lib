package se.snylt.particles;


import android.content.Context;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ParticlesWorld {

    private Rect bounds;

    private final float sizeUnit;

    private final HashMap<Class<? extends Particle>, List<Particle>> particles = new HashMap<>();

    private final List<ParticlesWorldController> controllers;

    ParticlesWorld(Context context, Rect bounds, ParticlesWorldController ...controllers) {
        this.bounds = bounds;
        this.controllers = new ArrayList<>(Arrays.asList(controllers));
        this.sizeUnit = context.getResources().getDisplayMetrics().density;

        for (ParticlesWorldController controller: controllers) {
            particles.put(controller.getParticleClass(), new ArrayList<Particle>());
        }
    }

    public Rect getBounds() {
        return bounds;
    }

    public float getSizeUnit() {
        return sizeUnit;
    }

    public List<Particle> getParticles(Class<? extends Particle> particleType) {
        return particles.get(particleType);
    }

    HashMap<Class<? extends Particle>, List<Particle>> getParticles() {
        return particles;
    }

    void onStart() {
        for(ParticlesWorldController controller:  controllers) {
            controller.onWorldStarted(this);
        }
    }

    void onUpdate(long dt) {
        updateThings(dt);
    }

    void onStop() {
        for(ParticlesWorldController controller:  controllers) {
            controller.onWorldStopped(this);
        }
    }

    private void updateThings(long dt) {
        for(ParticlesWorldController controller:  controllers) {
            controller.onUpdateWorld(this, particles.get(controller.getParticleClass()), dt);
        }
    }

    public void setBounds(int left, int top, int right, int bottom) {
        this.bounds = new Rect(left, top, right, bottom);
    }

}
