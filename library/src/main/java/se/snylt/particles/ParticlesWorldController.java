package se.snylt.particles;

import java.util.List;

public interface ParticlesWorldController<P extends Particle> {
    void onWorldStarted(ParticlesWorld particlesWorld);
    void onUpdateWorld(ParticlesWorld particlesWorld, List<P> particles, long dt);
    void onWorldStopped(ParticlesWorld particlesWorld);
    Class<P> getParticleClass();
}
