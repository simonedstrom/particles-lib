package se.snylt.particles;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.List;

public class ParticlesDrawable extends Drawable {

    private final ParticlesWorld particlesWorld;

    private UpdateRunner runner;

    private long updateFrequency = 50;


    public ParticlesDrawable(Context context, ParticlesWorldController<? extends Particle>  ...controllers) {
        this.particlesWorld = new ParticlesWorld(context, getBounds(), controllers);
    }

    public void start() {
        runner = new UpdateRunner(particlesWorld, this, updateFrequency);
        startIfBoundsAndNotStarted();
    }

    void startIfBoundsAndNotStarted(){
        if (getBounds().width() > 0 && getBounds().height() > 0 && runner != null) {
            runner.onStart();
        }
     }

    public void stop() {
        if(runner != null) {
            runner.onStop();
            runner = null;
        }
    }

    public long getUpdateFrequency() {
        return updateFrequency;
    }

    public void setUpdateFrequency(long updateFrequency) {
        this.updateFrequency = updateFrequency;
    }

    @Override
    public void draw(@NonNull final Canvas canvas) {
        HashMap<Class<? extends Particle>, List<Particle>> particlesMap = particlesWorld.getParticles();
        canvas.save();
        canvas.translate(getBounds().left, getBounds().top);
        for (Class<? extends Particle> key: particlesMap.keySet()) {
            List<? extends Particle> particles = particlesMap.get(key);
            for (int i = particles.size()-1; i>= 0; i--) {
                Particle particle = particles.get(i);
                canvas.save();
                particle.onDraw(canvas);
                canvas.restore();
            }
        }
        canvas.restore();
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        particlesWorld.setBounds(0, 0, right - left, bottom - top);
        startIfBoundsAndNotStarted();
    }

    @Override
    public void setAlpha(@IntRange(from = 0, to = 255) int alpha) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSPARENT;
    }
}
